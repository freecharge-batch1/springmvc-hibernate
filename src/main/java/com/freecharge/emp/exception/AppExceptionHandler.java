package com.freecharge.emp.exception;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AppExceptionHandler {

    @ExceptionHandler(value= EmployeeAlreadyExistsException.class)
        public String employeeAlreadyExistsExceptionHandler(Model model){
        model.addAttribute("msg","employee already exists with this details");
        return "error";
    }
}
