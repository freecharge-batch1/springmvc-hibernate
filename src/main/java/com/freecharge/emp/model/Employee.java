package com.freecharge.emp.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Component
@Data
@NoArgsConstructor
@Entity
@Table(name = "emp_details")
public class Employee {

    @Id
    @Column(name = "emp_id")
    private int id;
    @Column(name = "emp_name")
    private String name;
    @Column(name = "emp_email")
    private String email;
    @Column(name = "emp_contactno")
    private String contactNo;
}
