package com.freecharge.emp.repository;

import com.freecharge.emp.exception.EmployeeAlreadyExistsException;
import com.freecharge.emp.exception.EmployeeNotFoundException;
import com.freecharge.emp.model.Employee;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class EmployeeRepository {


    SessionFactory sessionFactory;
    @Autowired
    public EmployeeRepository(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public List<Employee> getEmpList(){
        /* HQL (Hibernate Query Language )
         * select * from emp-details
         * */
        return sessionFactory.getCurrentSession().createQuery("from Employee").list();

    }

    public Employee addEmployee(Employee employee) throws EmployeeAlreadyExistsException {
       /* Optional<Employee> empById = this.empList.stream().filter(emp -> emp.getId() == employee.getId()).findFirst();
        if(empById.isPresent())
            throw new EmployeeAlreadyExistsException("employee already exists with this id:" + employee.getId());
        this.empList.add(employee);*/
        Employee empById =sessionFactory.getCurrentSession().get(Employee.class,employee.getId());
        if(empById != null)
            throw new EmployeeAlreadyExistsException("employee already exists with this id:");

         sessionFactory.getCurrentSession().save(employee);
        return employee;
    }

    public boolean deleteEmployee(int id) throws EmployeeNotFoundException {
        //busines logic to remove
       /* Optional<Employee> empById = this.empList.stream().filter(emp -> emp.getId() == id).findFirst();
        if(!empById.isPresent())
            throw new EmployeeNotFoundException("employee not found with this id :" + id);
        this.empList.removeIf(emp -> emp.getId() == id);*/
        Employee empById =sessionFactory.getCurrentSession().get(Employee.class,id);
        if(empById == null)
            throw new EmployeeNotFoundException("employee doesn't exists with this id:");
        sessionFactory.getCurrentSession().delete(empById);
        return true;
    }
}
